drc-sim
=========
1. Simulates Wii U gamepad (DRC) on PC.
2. I assumes you have managed to connect your PC to Wii U. Wii U uses obfuscated WiFi and WPA to pair gamepage with 

Requirements
============
drc-sim depends on below Python packages

Package        Version
-------------- ---------
cffi           1.14.4
construct      2.10.56
PyAudio        0.2.11
pygame         2.0.1
setuptools     51.1.1

VS Studio 2019 with C++ SDK is required to complie above python packages.

I was managed to compile and run it with ffmpeg v58.91.100.0

How to run it
==============
1. Install python packages
pip install cffi
pip install construct
pip install pygame
pip install pipwin
pipwin install pyaudio
2. Download ffmpeg full-shared from https://www.gyan.dev/ffmpeg/builds/
3. Run python drc-sim.py. It will generate c code, pyc and pyd inside __pycache__. Copy all ffmpeg dll and exe from downloaded zip \bin to __pycache__.
4. Run drc-sim.py again.

To-Do
=======
1. Fix construct Switch and Embed.
2. Find out - how to make Windows connect to Wii U.